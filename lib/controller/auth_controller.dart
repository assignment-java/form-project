// ignore_for_file: await_only_futures, avoid_print, unnecessary_null_comparison, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:form_project/data/model/body/auth_model.dart';
import 'package:form_project/data/model/respone/user_model.dart';
import 'package:form_project/data/repository/auth_repository.dart';
import 'package:form_project/util/loading_dialog.dart';
import 'package:form_project/util/next_screen.dart';
import 'package:form_project/view/app/app_screen.dart';
import 'package:form_project/view/custom/show_custom_snakbar.dart';
import 'package:form_project/view/screen/account/set_password_screen.dart';
import 'package:form_project/view/screen/account/signin_screen.dart';
import 'package:form_project/view/screen/account/verification_code_screen.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AuthController extends GetxController implements GetxService {

  final AuthRepository authRepository;
  final SharedPreferences sharedPreferences;
  AuthController({required this.authRepository, required this.sharedPreferences});

  // set
  bool _isLoading = false;
  bool _isTypingCompleted = false;
  bool _isLoginWithPassword = false;
  bool _isPhoneNum = false;
  bool _isPassword = false;
  bool _isConfirmPassword = false;
  bool _isEnableVerificationCode = false;
  String _verificationCode = "";

  UserInfoModel? _userInfoModel;
  Map<String, dynamic>? _data;

  //get
  bool get isLoading => _isLoading;
  bool get isTypingCompleted => _isTypingCompleted;
  bool get isLoginWithPassword => _isLoginWithPassword;
  bool get isPhoneNum => _isPhoneNum;
  bool get isPassword => _isPassword;
  bool get isConfirmPassword => _isConfirmPassword;
  bool get isEnableVerificationCode => _isEnableVerificationCode;
  String get verificationCode => _verificationCode;

  UserInfoModel? get userInfoModel => _userInfoModel;
  Map<String, dynamic>? get data => _data;

  String? token;
  final String key = "secure_basicInfo_key";
  static const String noCache = "noCache";

  setTypingCompleted(bool isCompleted) {
    _isTypingCompleted = isCompleted;
    update();
  }

  setPhoneNum(bool isPhoneNum) {
    _isPhoneNum = isPhoneNum;
    update();
  }

  setPassword(bool isPassword) {
    _isPassword = isPassword;
    update();
  }

  setConfirmPassword(bool isConfirmPassword) {
    _isConfirmPassword = isConfirmPassword;
    update();
  }

  updateVerificationCode(String query) {
    if (query.length == 6) {
      _isEnableVerificationCode = true;
    } else {
      _isEnableVerificationCode = false;
    }
    _verificationCode = query;
    update();
  }

  Future<void> setLogin() async {
    _isLoginWithPassword = false;
  }

  void setCache(Map<String, dynamic> map) async {
    const storage = FlutterSecureStorage();
    storage.write(key: key, value: map.toString());
  }

  Future<String> getCache() async {
    const storage = FlutterSecureStorage();
    String? content = await storage.read(key: key);
    return content ?? noCache;
  }

  // Todo: phoneSignIn
  Future phoneSignIn(AuthModel authModel, context) async {

    // open command for sign in
    /*
    loadingDialogs(context);
    Response apiResponse = await authRepository.signInWithPhone(authModel);
    if (apiResponse.statusCode == 200) {
      if (apiResponse.body['message'] == "OK") {
        Navigator.pop(context);
        Map map = apiResponse.body;
        String token = '';
        showCustomSnackBar("Successful login", context, isError: false);
        try {
          token = map['data']["token"];
        } catch (e) {
          print(e.toString());
        }
        if (token != null && token.isNotEmpty) {
          authRepository.saveUserToken(token);
        }
        nextScreenNoReturn(context, AppScreen());
      } else {
        showCustomSnackBar("Error number or password", context);
        Navigator.pop(context);
      }
    }else {
      showCustomSnackBar("Server Down", context);
      Navigator.pop(context);
    }
    */
    // test for sign in
    nextScreenNoReturn(context, AppScreen());
    update();
  }

  // Todo: phoneSignUp
  Future phoneSignUp(String phone, String phoneCode, String verificationCode, String password, context) async {

    // open command for sign up
    /*
    _isLoading = true;
    update();
    Response apiResponse = await authRepository.signUpWithPhone(phone, phoneCode, verificationCode, password);
    if (apiResponse.statusCode == 200 && apiResponse.body['message'] == "OK") {
      Map map = apiResponse.body;
      String token = '';
      String message = '';
      try {
        message = map["message"];
        print("Message: $message");
      } catch (e) {
        print(e.toString());
      }
      try {
        token = map['data']["token"];
      } catch (e) {
        print(e.toString());
      }
      if (token != null && token.isNotEmpty) {
        authRepository.saveUserToken(token);
      }
      showCustomSnackBar('Successful create account', context, isError: false);
      nextScreenNoReturn(context, SignINScreen());
      _isLoading = false;
      update();
    } else {
      showCustomSnackBar('The phone has already been taken.', context, isError: true);
      if (apiResponse.hasError is String) {
        _isLoading = false;
        update();
      } else {
        _isLoading = false;
        update();
      }
      _isLoading = false;
      update();
    }
    _isLoading = false;
    */

    // test for sign up
    nextScreenNoReturn(context, AppScreen());
    update();
  }

  // Todo: sendOtp
  Future sendOtp(context, String phone, String phoneCode) async {

    // open command for send otp
    /*
    _isLoading = true;
    update();
    Response apiResponse = await authRepository.sendOTP(phone, phoneCode);
    if (apiResponse.statusCode == 200 && apiResponse.body['message'] == 'OK') {
      _isLoading = false;
      update();
      nextScreen(context, VerificationCodeScreen(phone: phone, phoneCode: phoneCode));
    }else if (apiResponse.statusCode == 200 && apiResponse.body['message'] != 'OK'){
      showCustomSnackBar('${apiResponse.body['message']}', context);
    } else {
      print(apiResponse.status);
    }
    _isLoading = false;
    */

    // test for send otp
    nextScreen(context, VerificationCodeScreen(phone: phone, phoneCode: phoneCode));
    update();
  }

  Future verifyOtp(context, String phone, String phoneCode, String otp) async {

    // open command for verify otp
    /*
    _isLoading = true;
    update();
    Response apiResponse = await authRepository.verifyOtp(phone, phoneCode, otp);
    if (apiResponse.statusCode == 200 && apiResponse.body['message'] == 'OK') {
      _isLoading = false;
      update();
      nextScreen(context, SetPasswordScreen(phone: phone, phoneCode: phoneCode, verifyCode: otp,));
    } else if(apiResponse.statusCode == 200 && apiResponse.body['message'] != 'OK') {
      showCustomSnackBar('${apiResponse.body['message']}', context);
    } else {
      showCustomSnackBar("Wrong number otp", context);
    }
    _isLoading = false;
    */

    // test for verify otp
    nextScreen(context, SetPasswordScreen(phone: phone, phoneCode: phoneCode, verifyCode: otp,));
    update();
  }

  // Todo: getUserInfo
  Future getUserInfo() async {
    Response apiResponse = await authRepository.getUserInfo();
    if (apiResponse.statusCode == 200) {
      _userInfoModel = UserInfoModel.fromJson(apiResponse.body);
    } else {
      print(apiResponse);
    }
    update();
  }

  // Todo: clearSharedData
  Future<bool> clearSharedData() async {
    return await authRepository.clearSharedData();
  }

  Future<void> getNetworkIP() async {
    var response = await http.get(Uri.parse('https://api.ipify.org'));
    if (response.statusCode == 200) {
      print('IP Address: ${response.body}');
      update();
    } else {
      throw Exception('Failed to get network IP');
    }
  }
}
