class AuthModel {
  String? id;
  String? uuid;
  String? name;
  String? email;
  String? phoneCode;
  String? phone;
  String? verificationCode;
  String? statusText;
  String? avatar;
  String? language;
  String? password;
  String? confirmPassword;

  AuthModel({
    this.id,
    this.uuid,
    this.name,
    this.email,
    this.phoneCode,
    this.phone,
    this.verificationCode,
    this.statusText,
    this.avatar,
    this.language,
    this.password,
    this.confirmPassword
  });

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['uuid'] = uuid;
    data['name'] = name;
    data['email'] = email;
    data['phone_code'] = phoneCode;
    data['phone'] = phone;
    data['verification_code'] = verificationCode;
    data['status_text'] = statusText;
    data['avatar'] = avatar;
    data['language'] = language;
    data['password'] = password;
    data['confirm_password'] = confirmPassword;
    return data;
  }
}