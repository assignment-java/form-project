class UserInfoModel {
  int? status;
  String? message;
  UserModel? data;

  UserInfoModel({this.status, this.message, this.data});

  UserInfoModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? UserModel.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['status'] = status;
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class UserModel {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? phoneCode;
  String? avatar;
  String? language;

  UserModel({this.id, this.name, this.email, this.phone, this.phoneCode, this.avatar, this.language,});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'].toString();
    phone = json['phone'];
    phoneCode = json['phone_code'];
    avatar = json['avatar'];
    language = json['language'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['email'] = email;
    data['phone'] = phone;
    data['phone_code'] = phoneCode;
    data['avatar'] = avatar;
    data['language'] = language;
    return data;
  }
}