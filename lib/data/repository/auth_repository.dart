// ignore_for_file: prefer_final_fields

import 'package:form_project/data/api/api_client.dart';
import 'package:form_project/data/model/body/auth_model.dart';
import 'package:form_project/data/model/body/device_body.dart';
import 'package:form_project/util/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get_connect/http/src/response/response.dart';

class AuthRepository {
  final DioClient dioClient;
  final SharedPreferences sharedPreferences;

  AuthRepository({required this.dioClient, required this.sharedPreferences});

  Future<void> saveUserToken(String token) async {
    dioClient.updateHeader(token);

    try {
      await sharedPreferences.setString(AppConstants.token, token);
    } catch (e) {
      throw e.toString();
    }
  }

  bool isLoggedIn() {
    return sharedPreferences.containsKey(AppConstants.token);
  }

  Future<Response> signUpWithPhone(String phone, String phoneCode, String verificationCode, String password) async {
    Map<String, String> body = {
      "phone": phone,
      "phone_code": phoneCode,
      "verification_code": verificationCode,
      "password": password,
    };
    try {
      Response response = await dioClient.postData(
        AppConstants.phoneSignUP, body,
        headers: {
          'Content-Type': 'application/json',
        }
      );
      return response;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<Response> signInWithPhone(AuthModel authModel) async {
    try {
      Response response = await dioClient.postData(AppConstants.phoneSignIn, authModel.toJson());
      return response;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<Response> getUserInfo() async {
    String? token = sharedPreferences.getString(AppConstants.token);
    try {
      final response = await dioClient.getData(AppConstants.getUserInfo,headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token'
      });
      return response;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<bool> clearSharedData() async {
    sharedPreferences.remove(AppConstants.token);
    return true;
  }

  Future<Response> sendOTP(String phone, String phoneCode) async {
    Map<String, String> body = {
      "phone": phone,
      "phone_code": phoneCode.substring(1)
    };
    try {
      final response = await dioClient.postData(
        AppConstants.sendVerificationSMS, body,
        headers: {'Content-Type': 'application/json'}
      );
      return response;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<Response> verifyOtp(String phone, String phoneCode, String otpCode) async {
    Map<String, String> body = {
      "phone": phone,
      "phone_code": phoneCode,
      "verification_code": otpCode
    };
    try {
      final response = await dioClient.postData(
        AppConstants.verificationCode, body,
        headers: {'Content-Type': 'application/json'}
      );
      return response;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<Response> verificationPinCode(String pinCode) async {
    Map<String, String> body = {
      "pin_code": pinCode,
    };
    try {
      final response = await dioClient.postData(AppConstants.verificationPinCode, body);
      return response;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<Response> signOut(String deviceID) async {
    try {
      final response = await dioClient.postData(
        AppConstants.signOut,
        {
          "device_token": deviceID
        }
      );
      return response;
    } catch (e) {
      throw e.toString();
    }
  }

  Future<Response> getDeviceInfo(DeviceInfo deviceInfo) async {
    try {
      final response = await dioClient.postData(AppConstants.deviceInfo, deviceInfo.toJson(),);
      return response;
    } catch (e) {
      throw e.toString();
    }
  }
}
