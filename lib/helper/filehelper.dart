import 'dart:io';
import 'package:image_picker/image_picker.dart';

class Helper {
  static void imgFromCamera(Function(File image) onPicked) async {
    final picker = ImagePicker();
    PickedFile? imageP = await picker.getImage(source: ImageSource.camera, imageQuality: 50);
    final File image = File(imageP!.path);
    onPicked(image);
  }
}

