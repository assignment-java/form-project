// ignore_for_file: prefer_collection_literals, no_leading_underscores_for_local_identifiers

import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:form_project/controller/auth_controller.dart';
import 'package:form_project/controller/localization_controller.dart';
import 'package:form_project/controller/splash_controller.dart';
import 'package:form_project/controller/theme_controller.dart';
import 'package:form_project/data/api/api_client.dart';
import 'package:form_project/data/model/respone/language_model.dart';
import 'package:form_project/data/repository/auth_repository.dart';
import 'package:form_project/data/repository/language_repo.dart';
import 'package:form_project/helper/network_info.dart';
import 'package:form_project/util/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';

Future<Map<String, Map<String, String>>> init() async {
  // Core
  final sharedPreferences = await SharedPreferences.getInstance();
  Get.lazyPut(() => NetworkInfo(Get.find()));
  Get.lazyPut(() => sharedPreferences);
  Get.lazyPut(() => DioClient(appBaseUrl: AppConstants.baseURL, sharedPreferences: Get.find()));

  // Repository
  Get.lazyPut(() => AuthRepository(dioClient: Get.find(), sharedPreferences: Get.find()));
  Get.lazyPut(() => LanguageRepo());

  // Controller
  Get.lazyPut(() => AuthController(authRepository: Get.find(), sharedPreferences: Get.find()));
  Get.lazyPut(() => SplashController());
  Get.lazyPut(() => LocalizationController(sharedPreferences: Get.find(), dioClient: Get.find()));
  Get.lazyPut(() => ThemeController(sharedPreferences: Get.find()));


  // Retrieving localized data
  Map<String, Map<String, String>> languages = Map();
  for (LanguageModel languageModel in AppConstants.languages) {
    String jsonStringValues = await rootBundle.loadString('assets/languages/${languageModel.languageCode}.json');
    Map<String, dynamic> mappedJson = json.decode(jsonStringValues);
    Map<String, String> _json = Map();
    mappedJson.forEach((key, value) {
      _json[key] = value.toString();
    });
    languages['${languageModel.languageCode}_${languageModel.countryCode}'] = _json;
  }
  return languages;
}
