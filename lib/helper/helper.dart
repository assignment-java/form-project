import 'dart:typed_data';
import 'package:http/http.dart' as http;

class Helper {
  static String notedMessage(String msg) {
    return "================> $msg <================";
  }

  static Future<Uint8List> convertImagetoByte(String url) async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return response.bodyBytes;
    } else {
      throw Exception('Failed to load image');
    }
  }
}
