import 'dart:typed_data';
import 'package:http/http.dart' as http;

class LocalImageHelper {
  static Future<Uint8List> getImage(String url) async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return response.bodyBytes;
    } else {
      throw Exception('Failed to load image');
    }
  }

  static Future<String?> saveImageToDatabase(String url) async {
    try {
      final imageBytes = await getImage(url);
      return imageBytes.toString();
    } catch (e) {
      return null;
    } finally {}
  }
}
