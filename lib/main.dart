// ignore_for_file: prefer_const_constructors, avoid_print, no_leading_underscores_for_local_identifiers, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:form_project/controller/localization_controller.dart';
import 'package:form_project/controller/theme_controller.dart';
import 'package:form_project/theme/dark_theme.dart';
import 'package:form_project/theme/light_theme.dart';
import 'package:form_project/util/app_constants.dart';
import 'package:form_project/util/messages.dart';
import 'package:form_project/view/screen/splash/splash_screen.dart';
import 'package:get/get.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'helper/get_di.dart' as di;

Future<void> main(context) async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  if (Platform.isAndroid) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.black));
  }

  HttpOverrides.global = MyHttpOverrides();
  Map<String, Map<String, String>> _languages = await di.init();
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark
    )
  );
  runApp(MyApp(
    languages: _languages
  ));
}

class MyApp extends StatefulWidget {

  final Map<String, Map<String, String>> languages;
  const MyApp({super.key, required this.languages});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed || state == AppLifecycleState.inactive) {
      print("state open: $state");
      FlutterAppBadger.removeBadge();
    }
    //out app
    if (state == AppLifecycleState.paused) {
      print("state: $state");
    }
    //close app
    if (state == AppLifecycleState.detached) {
      print("state: $state");
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(builder: (themeController) {
      return GetBuilder<LocalizationController>(builder: (localizeController) {
        return GetMaterialApp(
          title: AppConstants.appName,
          debugShowCheckedModeBanner: false,
          navigatorKey: Get.key,
          theme: themeController.darkTheme ? dark : light,
          translations: Messages(languages: widget.languages),
          locale: localizeController.locale,
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            Locale(AppConstants.languages[0].languageCode!, AppConstants.languages[0].countryCode),
            Locale(AppConstants.languages[1].languageCode!, AppConstants.languages[1].countryCode),
          ],
          localeListResolutionCallback: (locales, supportedLocales) {
            for (var locale in locales!) {
              if (supportedLocales.contains(locale)) {
                return locale;
              }
            }
            return supportedLocales.first;
          },
          fallbackLocale: Locale(AppConstants.languages[0].languageCode!, AppConstants.languages[0].countryCode),
          home: SplashScreen(),
        );
      });
    });
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}
