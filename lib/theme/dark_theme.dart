import 'package:flutter/material.dart';

ThemeData dark = ThemeData(
  fontFamily: 'Mulish',
  primaryColor: Color(0xFF27AE60),
  brightness: Brightness.dark,
  highlightColor: Colors.black,
  hintColor: Colors.grey.shade100,
  backgroundColor: Colors.black,
  pageTransitionsTheme: PageTransitionsTheme(builders: {
    TargetPlatform.android: ZoomPageTransitionsBuilder(),
    TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
    TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
  }),
);
