import 'package:flutter/material.dart';

ThemeData light = ThemeData(
  textTheme: TextTheme(
    headline1: TextStyle(color: Colors.black),
    headline2: TextStyle(color: Colors.black),
    bodyText2: TextStyle(color: Colors.black),
    subtitle1: TextStyle(color: Colors.black),
  ),
  fontFamily: 'Mulish',
  primaryColor: Color(0xFFE1BB17),
  backgroundColor: Colors.white,
  brightness: Brightness.light,
  highlightColor: Colors.white,
  hintColor: Colors.grey.shade400,
  pageTransitionsTheme: PageTransitionsTheme(builders: {
    TargetPlatform.android: ZoomPageTransitionsBuilder(),
    TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
    TargetPlatform.fuchsia: ZoomPageTransitionsBuilder(),
  }),
);
