// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:form_project/util/color_resources.dart';
import 'package:form_project/util/style.dart';
import 'package:get/get.dart';

class CustomAlertDialog {
  static void alertDiaLog(BuildContext context, {required String title, required String content, void Function()? onTap,bool? isChangePhone}) {
    showCupertinoDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title,style: textStyleBlack.copyWith(fontSize: 15),),
        content: Text(content),
        actions: <Widget>[
          CupertinoDialogAction(
            child: Text(
              'cancel'.tr,
              style: textStyleMedium.copyWith(color: Color(0xff27AE60)),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          isChangePhone == true ? CupertinoDialogAction(
            onPressed: onTap,
            child: Text(
              'ok'.tr,
              style: textStyleMedium.copyWith(color: Colors.black),
            ),
          ) : CupertinoDialogAction(
            onPressed: onTap,
            child: Text(
              'delete'.tr,
              style:
              textStyleMedium.copyWith(color: ColorResources.errorColor),
            )
          ),
        ],
      ),
    );
  }
}
