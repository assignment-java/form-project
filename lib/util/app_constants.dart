import 'package:form_project/data/model/respone/language_model.dart';

class AppConstants {
  static const String theme = "";
  static const String appName = "";
  static const String baseURL = "https://api-develop.dev";
  static const String token = 'token';
  static const String countryCode = 'country_code';
  static const String logo = 'assets/images/account.png';
  static const String languageCode = 'language_code';
  static const String deviceName = "device";
  static const String deviceToken = "device";
  static const String fmcToken = "device";
  static const String ipAddress = "device";


  // auth
  static const String phoneSignIn = "/api/signin/phone";
  static const String sendVerificationSMS = "/api/send-phone-code";
  static const String verificationCode = "/api/verify-phone-code";
  static const String verificationPinCode = "/api/verify-pin-code";
  static const String phoneSignUP = "/api/signup-by-phone";
  static const String signOut = "/api/signout";
  static const String getUserInfo = "/api/user/info";

  //device
  static const String deviceInfo = "/api/v1/front/device/info";

  static List<LanguageModel> languages = [
    LanguageModel(
        imageUrl: 'assets/images/logo_english.png',
        languageName: 'English',
        countryCode: 'US',
        languageCode: 'en'
    ),
    LanguageModel(
        imageUrl: 'assets/images/logo_china.png',
        languageName: '简体中文',
        countryCode: 'CN',
        languageCode: 'zh'
    )
  ];
}
