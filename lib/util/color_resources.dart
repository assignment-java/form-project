// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class ColorResources {
  static Color successColor =  Color(0xff27AE60);
  static Color warningColor =  Color(0xffE2B93B);
  static Color errorColor = Color(0xffEB5757);
  static Color violetColor = Color(0xff2B2F7E);
  static Color blueGreyColor = Color(0xffF5F6F8);
  static Color orangeColor = Color(0xffEA5D1A);
}