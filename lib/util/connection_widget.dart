// ignore_for_file: prefer_const_constructors

import 'package:form_project/util/color_resources.dart';
import 'package:form_project/util/dimensions.dart';
import 'package:form_project/util/style.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class ConnectionWidget extends StatefulWidget {

  bool? isConnect;
  ConnectionWidget({super.key, this.isConnect});

  @override
  State<ConnectionWidget> createState() => _ConnectionWidgetState();
}

class _ConnectionWidgetState extends State<ConnectionWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: Get.width,
          height: 55,
          color: widget.isConnect! ? ColorResources.successColor : ColorResources.errorColor,
          child: Center(
            child: SizedBox(
              width: Get.width * 0.95,
              child: Row(
                children: [
                  ImageIcon(
                    AssetImage(widget.isConnect! ? 'assets/images/wifi.png' : 'assets/images/no_wifi.png'),
                    color: Colors.white,
                  ),
                  SizedBox(width: Dimensions.paddingSizeDefault,),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.isConnect! ? 'Connected!' : 'No Connection',
                        style: textStyleLowMedium.copyWith(color: Colors.white, fontSize: 14),
                      ),
                      Text(
                       widget.isConnect! ? 'Your connection is back.' : 'Please check your connection ',
                        style: textStyleLowMedium.copyWith(color: Colors.white, fontSize: 12),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Expanded(
          child: Container(color: Colors.black.withOpacity(0.1),)
        )
      ],
    );
  }
}
