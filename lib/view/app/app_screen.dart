// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:form_project/controller/splash_controller.dart';
import 'package:form_project/util/color_resources.dart';
import 'package:form_project/view/screen/history/history_screen.dart';
import 'package:form_project/view/screen/home/home_screen.dart';
import 'package:form_project/view/screen/profile/profile_screen.dart';
import 'package:form_project/view/screen/video/video_screen.dart';
import 'package:get/get.dart';

class AppScreen extends StatefulWidget {
  const AppScreen({super.key});

  @override
  State<AppScreen> createState() => _AppScreenState();
}

class _AppScreenState extends State<AppScreen> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Widget>? listScreen;
  final PageController _pageController = PageController();
  final HomeScreen _homeScreen = HomeScreen();
  final VideoScreen _cardScreen = VideoScreen();
  final HistoryScreen _checkScreen = HistoryScreen();
  final ProfileScreen _profileScreen = ProfileScreen();
  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    listScreen = [
      _homeScreen,
      _cardScreen,
      _checkScreen,
      _profileScreen,
    ];
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 4,
      child: GetBuilder<SplashController>(builder: (splash) {
        return Scaffold(
          key: _scaffoldKey,
          appBar: _buildAppBar,
          body: _buildBody(splash),
          bottomNavigationBar: _buildBottomNavigationBar,
        );
      }),
    );
  }

  AppBar get _buildAppBar {
    return AppBar(
      title: Text(
        'Form Project'.tr,
        style: TextStyle(color: Colors.black),
      ),
      centerTitle: true,
    );
  }

  Widget _buildBody(splash) {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: _pageController,
      onPageChanged: (index) {
        splash.changeIndex(index);
      },
      children: listScreen!,
    );
  }

  // TODO: buildBottomNavigationBar
  Widget get _buildBottomNavigationBar{
    return BottomNavigationBar(
      selectedItemColor: ColorResources.orangeColor,
      unselectedItemColor: Colors.black54,
      currentIndex: _currentIndex,
      onTap: (index){
        setState(() {
          _currentIndex = index;
        });
        _pageController.jumpToPage(_currentIndex);
      },
      type: BottomNavigationBarType.fixed,
      items: <BottomNavigationBarItem> [
        BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home"
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.video_camera_back),
            label: "Video"
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.history),
            label: "History"
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.person_pin),
            label: "Profile"
        ),
      ],
    );
  }
}
