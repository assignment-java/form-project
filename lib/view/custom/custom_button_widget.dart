import 'package:flutter/material.dart';
import 'package:form_project/util/color_resources.dart';

class CustomButtonWidget{

  // Todo: buildButtonClick
  static Widget buildButtonClick({required String title, required bool activeColor, void Function()? onPress}){
    return SizedBox(
      width: double.infinity,
      height: 45,
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            side: const BorderSide(width: 0, color: Colors.transparent),
            backgroundColor: activeColor ? ColorResources.orangeColor : Colors.white,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          ),
          onPressed: onPress,
          child: Text(
            title,
            style: TextStyle(color: activeColor ? Colors.white : ColorResources.violetColor, fontSize: 18, fontWeight: FontWeight.w300),
          )
      ),
    );
  }
}