// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:form_project/controller/auth_controller.dart';
import 'package:form_project/util/color_resources.dart';
import 'package:form_project/util/dimensions.dart';

class CustomTextFieldWidget extends StatefulWidget {

  final TextEditingController textEditingController;
  final String? label;
  final bool isPhoneNumber;
  final bool isShowSuffix;
  final bool isClear;
  final bool isObscureText;
  final BorderSide border;
  final int maxLine;
  final Function(String value)? onChange;
  final Function()? onPress;
  final AuthController? authController;
  bool isDone;

  CustomTextFieldWidget(
    this.textEditingController,
    {
      super.key,
      this.label,
      this.isPhoneNumber = true,
      this.isShowSuffix = false,
      this.isClear = false,
      this.isObscureText = false,
      this.border = const BorderSide(color: Colors.black),
      this.maxLine = 1,
      this.onChange,
      this.onPress,
      this.authController,
      this.isDone = false
    }
  );

  @override
  State<CustomTextFieldWidget> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return Form(
      child: TextFormField(
        controller: widget.textEditingController,
        keyboardType: widget.isPhoneNumber? TextInputType.phone : TextInputType.text,
        validator: ((value) {
          widget.isPhoneNumber ? CustomValidatorWidget().validateMobile(value!) : null;
          return null;
        }),
        maxLines: widget.maxLine,
        onChanged: widget.onChange,
        obscureText: widget.isObscureText,
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(vertical: 12),
          filled: widget.authController!.isTypingCompleted ? false : true,
          fillColor: widget.textEditingController.text.isEmpty ? ColorResources.blueGreyColor : ColorResources.blueGreyColor,
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelStyle: const TextStyle(fontSize: 16),
          border: InputBorder.none,
          prefix: const Text("  "),
          hintText: widget.label,
          suffixIcon: widget.isShowSuffix ? IconButton(
            iconSize: 24,
            splashColor: Colors.transparent,
            icon: Icon(widget.isObscureText ? Icons.visibility : Icons.visibility_off, color: Colors.grey,),
            onPressed: widget.onPress,
          ) : null,
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.transparent),
            borderRadius: BorderRadius.circular(Dimensions.paddingSizeExtraSmall),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: widget.border,
            borderRadius: BorderRadius.circular(Dimensions.paddingSizeExtraSmall),
          ),
        ),
        cursorColor: Colors.black45,
      ),
    );
  }
}

class CustomValidatorWidget {
  String validateMobile(String value) {
    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = RegExp(pattern);
    if (value.isEmpty) {
      return 'Please enter mobile number';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid mobile number';
    }
    return "";
  }
}