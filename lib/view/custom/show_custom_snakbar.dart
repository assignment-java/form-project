import 'package:flutter/material.dart';
import 'package:form_project/util/color_resources.dart';
import 'package:form_project/util/dimensions.dart';
import 'package:form_project/util/style.dart';

void showCustomSnackBar(String message, BuildContext context, {bool isError = true, Color? colors}) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      behavior: SnackBarBehavior.floating,
      content: Text(
        message,
        style: robotoRegular.copyWith(fontSize: Dimensions.fontSizeSmall),
      ),
      backgroundColor: colors ?? (isError ? ColorResources.errorColor : ColorResources.successColor),
    ),
  );
}