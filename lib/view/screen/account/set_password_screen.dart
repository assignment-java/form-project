// ignore_for_file: avoid_print, prefer_const_literals_to_create_immutables, prefer_const_constructors, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_project/controller/auth_controller.dart';
import 'package:form_project/view/custom/custom_button_widget.dart';
import 'package:form_project/view/custom/custom_textfield_widget.dart';
import 'package:get/get.dart';

class SetPasswordScreen extends StatefulWidget {

  final String phone;
  final String phoneCode;
  final String verifyCode;
  SetPasswordScreen({super.key, required this.phone, required this.phoneCode, required this.verifyCode});

  @override
  State<SetPasswordScreen> createState() => _SetPasswordScreenState();
}

class _SetPasswordScreenState extends State<SetPasswordScreen> {

  // Todo: Properties
  final TextEditingController _passCtrl = TextEditingController();
  bool isShowPass = true;

  // Todo: Body
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(builder: (auth){
      return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
          body: _buildBody(auth),
        ),
      );
    });
  }

  // Todo: buildBody
  Widget _buildBody(AuthController auth) {
    return TextButton(
      style: ButtonStyle(overlayColor: MaterialStateColor.resolveWith((states) => Colors.transparent),),
      onPressed: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 120,),
              _buildImage,
              const SizedBox(height: 32,),
              _buildTextTitle,
              const SizedBox(height: 16,),
              _buildCheckPassword,
              const SizedBox(height: 32,),
              _buildPassword(auth),
              const SizedBox(height: 64,),
              CustomButtonWidget.buildButtonClick(
                title: "Done",
                activeColor: true,
                onPress: (_passCtrl.text.length < 4) ? null : (){
                  auth.phoneSignUp(
                    widget.phone,
                    widget.phoneCode,
                    widget.verifyCode,
                    _passCtrl.text,
                    context
                  );
                }
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Todo: buildImage
  Widget get _buildImage {
    return Container(
      height: 60,
      width: 60,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/account.png"),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  // Todo: buildTextTitle
  Widget get _buildTextTitle {
    return Row(
      children: [
        Text("Set Password", style: TextStyle(color: Colors.black, fontSize: 28, fontWeight: FontWeight.w500),),
        Spacer()
      ],
    );
  }

  // Todo: buildCheckPassword
  Widget get _buildCheckPassword {
    return Row(
      children: [
        Text(
          "Please set a password of more than 4 characters",
          style: TextStyle(color: Colors.grey, fontSize: 14, fontWeight: FontWeight.w400),
        ),
        Spacer()
      ],
    );
  }

  // Todo: buildPassword
  Widget _buildPassword(auth) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Password", style: TextStyle(color: Colors.black, fontSize: 16),),
        SizedBox(height: 8,),
        Stack(
          children: [
            CustomTextFieldWidget(
              _passCtrl,
              isPhoneNumber: false,
              label: "Enter password",
              isObscureText: isShowPass,
              isShowSuffix: true,
              border: BorderSide(color: auth.isPassword ? Colors.transparent : Colors.transparent),
              onPress: (){
                setState(() {
                  isShowPass = !isShowPass;
                });
              },
              authController: auth,
              onChange: (value){
                if (_passCtrl.text.isNotEmpty){
                  auth.setPassword(true);
                }else {
                  auth.setPassword(false);
                }
              },
            ),
          ],
        ),
      ],
    );
  }
}
