// ignore_for_file: avoid_print, prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_project/controller/auth_controller.dart';
import 'package:form_project/data/model/body/auth_model.dart';
import 'package:form_project/util/color_resources.dart';
import 'package:form_project/util/next_screen.dart';
import 'package:form_project/view/custom/custom_button_widget.dart';
import 'package:form_project/view/custom/custom_code_picker_widget.dart';
import 'package:form_project/view/custom/custom_textfield_widget.dart';
import 'package:form_project/view/screen/account/signup_screen.dart';
import 'package:get/get.dart';

class SignINScreen extends StatefulWidget {
  const SignINScreen({super.key});

  @override
  State<SignINScreen> createState() => _SignINScreenState();
}

class _SignINScreenState extends State<SignINScreen> {

  // Todo: Properties
  final TextEditingController _phoneCtrl = TextEditingController();
  final TextEditingController _passCtrl = TextEditingController();
  bool isCheck = false;
  bool isShowPass = true;
  String _countryDialCode = "+855";

  String getSubString(String number) {
    if(number[0] == '0'){
      return number.substring(1, number.length);
    }
    return number;
  }

  // Todo: Body
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(builder: (auth){
      return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
          body: _buildBody(auth),
        ),
      );
    });
  }

  // Todo: buildBody
  Widget _buildBody(auth) {
    return TextButton(
      style: ButtonStyle(overlayColor: MaterialStateColor.resolveWith((states) => Colors.transparent),),
      onPressed: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 120,),
              _buildImage,
              const SizedBox(height: 32,),
              _buildTextTitle,
              const SizedBox(height: 32,),
              _buildPhoneNumber(auth),
              const SizedBox(height: 16,),
              _buildPassword(auth),
              const SizedBox(height: 64,),
              CustomButtonWidget.buildButtonClick(
                title: "Done",
                activeColor: true,
                onPress: (_phoneCtrl.text.length < 8 || _passCtrl.text.length < 4) ? null : () {
                auth.phoneSignIn(
                  AuthModel(
                    phone: getSubString(_phoneCtrl.text),
                    phoneCode: _countryDialCode.substring(1),
                    password: _passCtrl.text,
                  ),
                  context
                );
                print("Country code: $_countryDialCode");
              }),
              _buildBtnSignUp,
            ],
          ),
        ),
      ),
    );
  }

  // Todo: buildImage
  Widget get _buildImage {
    return Container(
      height: 60,
      width: 60,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/account.png"),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  // Todo: buildTextTitle
  Widget get _buildTextTitle {
    return Row(
      children: [
        Text("Sign in", style: TextStyle(color: Colors.black, fontSize: 28, fontWeight: FontWeight.w500),),
        Spacer()
      ],
    );
  }

  // Todo: buildPhoneNumber
  Widget _buildPhoneNumber(auth) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Phone number", style: TextStyle(color: Colors.black, fontSize: 16)),
        const SizedBox(height: 8,),
        Container(
          alignment: Alignment.centerLeft,
          height: 50,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: ColorResources.blueGreyColor),
          child: Row(
            children: [
              SizedBox(
                height: 50,
                width: 135,
                child: CustomCodePickerWidget(
                  onChanged: (CountryCode countryCode) {
                    _countryDialCode = countryCode.dialCode!;
                  },
                  initialSelection: _countryDialCode,
                  favorite: [_countryDialCode],
                  showDropDownButton: true,
                  padding: EdgeInsets.zero,
                  showFlagMain: true,
                  textStyle: const TextStyle(color: Colors.black,),
                ),
              ),
              Container(
                height: 24,
                width: 2,
                color: Colors.black,
              ),
              Expanded(
                child: CustomTextFieldWidget(
                  _phoneCtrl,
                  label: "Enter phone number",
                  authController: auth,
                  border: BorderSide(color: auth.isPhoneNum ? Colors.transparent : Colors.transparent),
                  onChange: (value){
                    if (_phoneCtrl.text.isNotEmpty){
                      auth.setPhoneNum(true);
                    } else {
                      auth.setPhoneNum(false);
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  // Todo: buildPassword
  Widget _buildPassword(auth) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Password", style: TextStyle(color: Colors.black, fontSize: 16),),
        SizedBox(height: 8,),
        Stack(
          children: [
            CustomTextFieldWidget(
              _passCtrl,
              isPhoneNumber: false,
              label: "Enter password",
              isObscureText: isShowPass,
              isShowSuffix: true,
              border: BorderSide(color: Colors.transparent),
              onPress: (){
                setState(() {
                  isShowPass = !isShowPass;
                });
              },
              authController: auth,
              onChange: (value){
                if (_passCtrl.text.isNotEmpty){
                  auth.setPassword(true);
                }else {
                  auth.setPassword(false);
                }
              },
            ),
          ],
        ),
      ],
    );
  }

  // Todo: buildBtnSignUp
  Widget get _buildBtnSignUp {
    return Container(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
            onPressed: () {
              nextScreenReplace(context, SignUPScreen());
            },
            style: ButtonStyle(overlayColor: MaterialStateColor.resolveWith((states) => Colors.transparent),),
            child: Text(
              "Sign up",
              style: TextStyle(color: ColorResources.orangeColor, fontSize: 16, fontWeight: FontWeight.w400),
            ),
          ),
          Text("new account", style: TextStyle(color: Colors.grey, fontSize: 16),),
        ],
      ),
    );
  }
}
