// ignore_for_file: avoid_print, prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_project/controller/auth_controller.dart';
import 'package:form_project/util/color_resources.dart';
import 'package:form_project/util/next_screen.dart';
import 'package:form_project/view/custom/custom_button_widget.dart';
import 'package:form_project/view/custom/custom_code_picker_widget.dart';
import 'package:form_project/view/custom/custom_textfield_widget.dart';
import 'package:form_project/view/screen/account/signin_screen.dart';
import 'package:get/get.dart';

class SignUPScreen extends StatefulWidget {
  const SignUPScreen({super.key});

  @override
  State<SignUPScreen> createState() => _SignUPScreenState();
}

class _SignUPScreenState extends State<SignUPScreen> {

  // Todo: Properties
  final TextEditingController _phoneCtrl = TextEditingController();
  bool isCheck = false;
  bool isShowPass = true;
  String _countryDialCode = "+855";

  String getSubString(String number) {
    if(number[0] == '0'){
      return number.substring(1, number.length);
    }
    return number;
  }

  // Todo: Body
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(builder: (auth){
      return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
          body: _buildBody(auth),
        ),
      );
    });
  }

  // Todo: buildBody
  Widget _buildBody(auth) {
    return TextButton(
      style: ButtonStyle(overlayColor: MaterialStateColor.resolveWith((states) => Colors.transparent),),
      onPressed: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 120,),
              _buildImage,
              const SizedBox(height: 32,),
              _buildTextTitle,
              const SizedBox(height: 32,),
              _buildPhoneNumber(auth),
              const SizedBox(height: 8,),
              _buildCheckCircle,
              const SizedBox(height: 64,),
              CustomButtonWidget.buildButtonClick(
                title: "Next",
                activeColor: true,
                onPress: (_phoneCtrl.text.length < 8 || isCheck == false) ? null : () {
                  auth.sendOtp(
                    context,
                    getSubString(_phoneCtrl.text),
                    _countryDialCode,
                  );
                  print("Country code: $_countryDialCode");
                }
              ),
              _buildBtnSignUp,
            ],
          ),
        ),
      ),
    );
  }

  // Todo: buildImage
  Widget get _buildImage {
    return Container(
      height: 60,
      width: 60,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/account.png"),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  // Todo: buildTextTitle
  Widget get _buildTextTitle {
    return Row(
      children: [
        Text("Sign up", style: TextStyle(color: Colors.black, fontSize: 28, fontWeight: FontWeight.w500),),
        Spacer()
      ],
    );
  }

  // Todo: buildPhoneNumber
  Widget _buildPhoneNumber(auth) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Phone number", style: TextStyle(color: Colors.black, fontSize: 16)),
        const SizedBox(height: 8,),
        Container(
          alignment: Alignment.centerLeft,
          height: 50,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), color: ColorResources.blueGreyColor),
          child: Row(
            children: [
              SizedBox(
                height: 50,
                width: 135,
                child: CustomCodePickerWidget(
                  onChanged: (CountryCode countryCode) {
                    _countryDialCode = countryCode.dialCode!;
                  },
                  initialSelection: _countryDialCode,
                  favorite: [_countryDialCode],
                  showDropDownButton: true,
                  padding: EdgeInsets.zero,
                  showFlagMain: true,
                  textStyle: const TextStyle(color: Colors.black,),
                ),
              ),
              Container(
                height: 24,
                width: 2,
                color: Colors.black,
              ),
              Expanded(
                child: CustomTextFieldWidget(
                  _phoneCtrl,
                  label: "Enter phone number",
                  authController: auth,
                  border: BorderSide(color: auth.isPhoneNum ? Colors.transparent : Colors.transparent),
                  onChange: (value){
                    if (_phoneCtrl.text.isNotEmpty){
                      auth.setPhoneNum(true);
                      print("object: ${_phoneCtrl.text}");
                    } else {
                      auth.setPhoneNum(false);
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  // Todo: buildCheckCircle
  Widget get _buildCheckCircle {
    return GestureDetector(
      onTap: (){
        setState(() {
          isCheck = !isCheck;
        });
      },
      child: Row(
        children: [
          isCheck ? Image.asset("assets/icons/check_circle.png", height: 24, width: 24,) : Image.asset("assets/icons/circle.png", height: 24, width: 24,),
          SizedBox(width: 8,),
          Text("Agree to the user agreement", style: TextStyle(color: Colors.grey, fontSize: 16),),
        ],
      ),
    );
  }

  // Todo: buildBtnSignUp
  Widget get _buildBtnSignUp {
    return Container(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
            onPressed: () {
              nextScreenReplace(context, SignINScreen());
            },
            style: ButtonStyle(overlayColor: MaterialStateColor.resolveWith((states) => Colors.transparent),),
            child: Text(
              "Sign in",
              style: TextStyle(color: ColorResources.orangeColor, fontSize: 16, fontWeight: FontWeight.w400),
            ),
          ),
          Text("old account", style: TextStyle(color: Colors.grey, fontSize: 16),),
        ],
      ),
    );
  }
}
