// ignore_for_file: avoid_print, prefer_const_literals_to_create_immutables, prefer_const_constructors, prefer_const_constructors_in_immutables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:form_project/controller/auth_controller.dart';
import 'package:form_project/util/color_resources.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:get/get.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';

class VerificationCodeScreen extends StatefulWidget {

  final String phone;
  final String phoneCode;
  VerificationCodeScreen({super.key, required this.phone, required this.phoneCode});

  @override
  State<VerificationCodeScreen> createState() => _VerificationCodeScreenState();
}

class _VerificationCodeScreenState extends State<VerificationCodeScreen> {

  // Todo: Properties
  int number = 6;

  // Todo: Body
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(builder: (auth){
      return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
          body: _buildBody(auth),
        ),
      );
    });
  }

  // Todo: buildBody
  Widget _buildBody(auth) {
    return TextButton(
      style: ButtonStyle(overlayColor: MaterialStateColor.resolveWith((states) => Colors.transparent),),
      onPressed: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 120,),
              _buildImage,
              const SizedBox(height: 32,),
              _buildTextTitle,
              const SizedBox(height: 16,),
              _buildCheckPhone,
              const SizedBox(height: 44,),
              _buildPinCodeField(auth),
              _buildResendCode(auth),
            ],
          ),
        ),
      ),
    );
  }

  // Todo: buildImage
  Widget get _buildImage {
    return Container(
      height: 60,
      width: 60,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/account.png"),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  // Todo: buildTextTitle
  Widget get _buildTextTitle {
    return Row(
      children: [
        Text("Verification code", style: TextStyle(color: Colors.black, fontSize: 28, fontWeight: FontWeight.w500),),
        Spacer()
      ],
    );
  }

  // Todo: buildCheckPhone
  Widget get _buildCheckPhone {
    return Row(
      children: [
        Text(
          "Please check the phone ***${widget.phone.substring(widget.phone.length - 3)} SMS",
          style: TextStyle(color: Colors.grey, fontSize: 15, fontWeight: FontWeight.w400),
        ),
        Spacer()
      ],
    );
  }

  // Todo: buildPinCodeField
  Widget _buildPinCodeField(auth) {
    return PinCodeTextField(
      backgroundColor: Colors.grey.shade50,
      appContext: context,
      length: number,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(4),
        fieldHeight: 44,
        fieldWidth: 44,
        activeColor: ColorResources.orangeColor,
        selectedColor: ColorResources.orangeColor,
        inactiveColor: Colors.grey,
        activeFillColor: ColorResources.orangeColor,
        selectedFillColor: ColorResources.orangeColor,
        inactiveFillColor: ColorResources.blueGreyColor,
        borderWidth: 1
      ),
      cursorColor: ColorResources.orangeColor,
      keyboardType: TextInputType.phone,
      animationType: AnimationType.fade,
      onChanged: auth.updateVerificationCode,
      onCompleted: (String text) async {
        auth.verifyOtp(context, widget.phone,widget.phoneCode.substring(1), auth.verificationCode);
      },
    );
  }

  // Todo: buildResendCode
  Widget _buildResendCode(auth) {
    return CountdownTimer(
      endTime: DateTime.now().millisecondsSinceEpoch + 60000,
      widgetBuilder: (_, CurrentRemainingTime? time) {
        if (time == null) {
          return GestureDetector(
            onTap: () {
              auth.sendOtp(
                context,
                widget.phone,
                widget.phoneCode,
              );
            },
            child: Text("try again", style: TextStyle(color: ColorResources.orangeColor),)
          );
        }
        return Row(
          children: [
            Text("Resend in ", style: TextStyle(color: ColorResources.orangeColor),),
            Text("${time.sec ?? 0}s", style: TextStyle(color: ColorResources.orangeColor),),
          ],
        );
      },
    );
  }
}
