// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:form_project/controller/auth_controller.dart';
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody,
    );
  }

  Widget get _buildBody{
    return GetBuilder<AuthController>(
      builder: (auth){
        return Center(
          child: Text("Home Screen"),
        );
      },
    );
  }
}
