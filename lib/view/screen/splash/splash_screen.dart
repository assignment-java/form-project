// ignore_for_file: prefer_const_constructors, avoid_print

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:form_project/controller/auth_controller.dart';
import 'package:form_project/util/app_constants.dart';
import 'package:form_project/util/color_resources.dart';
import 'package:form_project/util/next_screen.dart';
import 'package:form_project/view/app/app_screen.dart';
import 'package:form_project/view/screen/account/signin_screen.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  SharedPreferences? sharedPreferences;
  afterSplash() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if(mounted){
      try {
        String token = sharedPreferences!.getString(AppConstants.token)!;
        if(token != ""){
          await Get.find<AuthController>().getUserInfo().then((value) {
            nextScreenReplace(context, AppScreen());
          });
        } else{
          nextScreenReplace(context, SignINScreen());
        }
      } catch (e) {
        nextScreenReplace(context, SignINScreen());
      }
    } else{

    }
  }

  askPerMission() async{
    if (Platform.isAndroid) {
      await Permission.storage.request();
      await Permission.contacts.request();
      await Permission.location.request();
      await Permission.photos.request();
      await Permission.camera.request();
      await Permission.microphone.request();
      if (await Permission.storage.isGranted &&
          await Permission.location.isGranted &&
          await Permission.camera.isGranted &&
          await Permission.photos.isGranted &&
          await Permission.contacts.isGranted &&
          await Permission.microphone.isGranted) {
        print('permission granted');
      } else {
        print('permission denied');
      }
    }
    else if (Platform.isIOS) {
      await Permission.contacts.request();
      await Permission.storage.request();
      await Permission.location.request();
      await Permission.camera.request();
      await Permission.photos.request();
      await Permission.microphone.request();
      if (await Permission.storage.isGranted &&
          await Permission.location.isGranted &&
          await Permission.camera.isGranted &&
          await Permission.contacts.isGranted &&
          await Permission.microphone.isGranted) {
        print('permission granted');
      } else {
        print('permission denied');
      }
    }
  }

  @override
  void initState() {
    askPerMission();
    afterSplash();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorResources.orangeColor,
      body: Center(
        child: Image(
          width: 110,
          height: 110,
          image: AssetImage(AppConstants.logo),
          fit: BoxFit.contain,
        )
      )
    );
  }
}
